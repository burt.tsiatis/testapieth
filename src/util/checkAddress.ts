const validate = (address: string): boolean => {
  return /^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address);
};

export {
  validate
};