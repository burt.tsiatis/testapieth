import BigNumber from 'bignumber.js';

const hex2dec = (hex: string): string => {
  const bn = new BigNumber(hex, 16);
  return bn.toString();
};

const dec2hex = (num: string): string => {
  const bn = new BigNumber(num);
  return bn.toString(16);
};

const sum = (hexNumbers: string[]): string => {
  return hexNumbers.reduce((p, c) => {
    return p + parseInt(c, 16);
  }, 0).toString(16);
};

const diff = (hexNumbers: string[]): string => {
  return hexNumbers.reduce(( p, c ) => {
    return (parseInt(p, 16) - parseInt(c, 16)).toString(16);
  });
};

export {
  hex2dec,
  dec2hex,
  sum,
  diff
};