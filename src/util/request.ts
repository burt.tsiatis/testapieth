import axios from 'axios';
import logger from '../util/logger';

const {log} = logger('request'); 

type Request = {
  url: string;
  headers: object;
  params?: object;
}

type RequestGet = {
  uri: string;
  method: string;
  headers?: object;
  data?: null | object;
  params?: object;
}

export default ({url, params: cammonParams={}, headers: commonHeaders={}}: Request) => {
  
  const request = async ({uri='', method='get', headers={}, data=null, params={}}: RequestGet) => {
    log.debug(`${method} ${url}${uri}`);
    const requestParams = {
      method,
      url: `${url}${uri}`,
      headers: {...commonHeaders,...headers},
      params: {...cammonParams, ...params},
      data
    };

    try {
      const res = await axios(requestParams);

      return res;
    } catch(e) {
      log.error(e);
      throw e;
    }
  };

  const get = async (uri = '', params: object) => request({uri, method: 'get', params});
  return { get };
};