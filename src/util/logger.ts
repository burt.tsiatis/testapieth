import { createLogger, format, transports } from 'winston';
import config from '../config';
const { combine, timestamp, colorize, label } = format;

const {
  app: {
    name,
    logLevel
  }
} = config;


export default (namespace: string) => {
  const createNewLogger = (namespace: string) => createLogger({
    level: logLevel,
    format: combine (
      colorize(),
      timestamp(),
      label({label: namespace}),
      format.printf(({ timestamp, level, label, message, service }) => {
        return `[${timestamp}] - [${service}:${label}] - ${level}: ${message}`;
      })
    ),
    defaultMeta: {
      service: name
    },
    transports: [new transports.Console()],
  });

  const log = createNewLogger(namespace);
  return {log};
};
