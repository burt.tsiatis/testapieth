const {
  APP_NAME,
  APP_ENV,
  APP_HOST,
  APP_PORT,
  APP_APIPATH,
  APP_LOGLEVEL,
  ETH_URL,
  ETH_API_KEY,
} = process.env;

export default {
  app: {
    name: APP_NAME || 'TestApiEth',
    environment: APP_ENV || 'debug',
    port: APP_PORT || 9091,
    host: APP_HOST || 'localhost',
    apiPath: APP_APIPATH || '/api/v1',
    logLevel: APP_LOGLEVEL || 'debug'
  },
  eth: {
    url: ETH_URL || 'https://api.etherscan.io',
    apiKey: ETH_API_KEY || 'DX7ZDV9IPUX8X5G9KK8QE4QX2PCYW5QSR8',
  }
};