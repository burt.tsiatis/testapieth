import { Router } from 'express';
import { getAddrFromBlocks } from './handler/getAddrFromBlocks';

const api = Router();

api.get('/getAddrFromBlocks', getAddrFromBlocks);

export default api;