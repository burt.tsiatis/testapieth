import { Request, Response } from 'express';
import Bottleneck from 'bottleneck';
import { AxiosResponse } from 'axios';
import config from '../config';
import { sum, diff, hex2dec, dec2hex } from '../util/converter';
import request from '../util/request';
import { validate } from '../util/checkAddress';

const { get } = request({
  url: config.eth.url,
  headers: {
    'Content-Type': 'application/json'
  },
});

const limiter = new Bottleneck({
  maxConcurrent: 4,
  minTime: 250
});
const wrapped = limiter.wrap(get);

import logger from '../util/logger';
const { log } = logger('getAddrFromBlock'); 

type AddressesMap = {
  [name: string]: string;
}

type Trx = {
  from: string;
  to: string;
  value: string;
  gas: string;
};

const getLastBlock = async () => {
  const params = {
    module: 'proxy',
    action: 'eth_blockNumber',
    apikey: config.eth.apiKey
  };

  const { data } = await wrapped('/api', params);

  return data.result;
};

interface IReqQuery<T> extends Request<unknown, unknown, unknown, T> {
  query: T;
}

type TQueey = {
  addressUnder: string;
  countBlocks: string;
}


export const getAddrFromBlocks = async(req: IReqQuery<TQueey>, res: Response) => {

  const { addressUnder, countBlocks } = req.query;

  if (!addressUnder || !validate(addressUnder)) {
    return res.status(400).json({
      status: '001',
      message: 'Wrong address'
    });
  }

  // TODO: Limit to count blocks
  if (!countBlocks || !+countBlocks) {
    return res.status(400).json({
      status: '002',
      message: 'Wrong param countBlocks'
    });
  }

  const start = performance.now();

  const params = {
    module: 'proxy',
    action: 'eth_getBlockByNumber',
    tag: '',
    boolean: true,
    apikey: config.eth.apiKey
  };

  const addressMap: AddressesMap = {};
  const lastBlock = hex2dec(await getLastBlock());
  
  const txs: Trx[] = [];

  log.debug(`last block - ${lastBlock}`);

  const tags: number[] = Array.from(
    { length: +countBlocks},
    (_, i: number) => +lastBlock - i
  );

  const tasks = tags.map(tag => {
    params.tag = dec2hex(tag.toString());
    return wrapped('/api', params);
  });

  await Promise.all(tasks).then((rsps: AxiosResponse[]) => {
    rsps.map(({ data }) => {
      if (!Object.keys(data).length) {
        log.error('Not found data');
        return res.status(400).json({
          status: '003',
          message: 'Not found data from api etherscan.io'
        });
      }
      if (!data.result) {
        log.error('Not found result form data');
        return;
      }
      if (data.status === '0') {
        log.error(data.result);
        return;
      }
  
      if (!data.result.transactions || !data.result.transactions.length) {
        log.warn('Not transactions in response');
        return;
      }
  
      txs.push(...data.result.transactions);
    });
  });

  if (!txs.length) {
    log.error('Not found txs');
    return res.status(400).json({
      status: '004',
      message: 'Not found txs'
    });
  }

  txs.map((trx: Trx) => {
    addressMap[trx.to] = sum([trx.value, trx.gas, addressMap[trx.to] || '0']);
    addressMap[trx.from] = diff([trx.value, trx.gas, addressMap[trx.to] || '0']);
  });

  log.debug(`Time duration - ${performance.now() - start}`);

  return res.status(200).json({
    result: {
      [addressUnder]: addressMap[addressUnder] || ''
    }
  });
};
