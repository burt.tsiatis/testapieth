import express, { Application } from 'express';
import cors from 'cors';
import helmet from 'helmet';
import swaggerUi from 'swagger-ui-express';
import * as swaggerDocument from '../swagger';
import logger from './util/logger';

const { log } = logger('index');

import api from './api';
import config from './config';

const {
  app: {
    environment,
    host,
    port,
    apiPath,
  }

} = config;


const app: Application = express();

const corsOptions = {
  origin: `http://${host}`
};

app.use(cors(corsOptions));
app.use(helmet());

app.use(apiPath, api);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port, () => {
  log.info(`Server started on port ${port} env ${environment}!`);
  log.info(`Use swagger on http://${host}:${port}/api-docs`);
});