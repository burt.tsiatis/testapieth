FROM node:16-alpine
WORKDIR /app
COPY . .
ENV APP_ENV dev
EXPOSE 9091
RUN npm install
RUN npm run build
CMD npm start
