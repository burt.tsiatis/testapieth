import { 
  name,
  version,
  description,
  license
} from './package.json';

import config from './src/config';

const {
  app: {
    apiPath,
    port,
    environment,
    host
  }
} = config;

module.exports = {
  openapi: '3.0.3',
  info: {
    version,
    title: `${name.split('-').map(w => w.charAt(0).toUpperCase() + w.slice(1)).join(' ')} `,
    description: `${description} (MODE: ${environment})`,
    license
  },
  host: 'http://${host}:${port}/api-docs',
  basePath: apiPath,
  servers: [
    { url: `http://${host}:${port}${apiPath}` }
  ],
  tags: [{
    name: 'API'
  }],
  schemes: [
    'http'
  ],
  paths: {
    '/getAddrFromBlocks': {
      get: {
        tags: ['API'],
        summary: 'Get balance on address from eth blockchain some count blocks ago',
        description: 'Get balance on address from eth blockchain some count blocks ago',
        produces:[  
          'application/json'
        ],
        parameters:[  
          {  
            'name':'addressUnder',
            'in':'query',
            'description':'Eth address',
            'required':true,
            'type':'string'
          },
          {  
            'name':'countBlocks',
            'in':'query',
            'description':'Count blocks ago',
            'required':true,
            'type':'string'
          },
        ],
        responses: {
          200:{  
            description:'OK',
            content: {
              // eslint-disable-next-line quotes
              "application/json": {
                examples: {
                  success: {
                    value : {
                      result: {
                        address: 'valueInHex'
                      }
                    }
                  }
                }
              }
            },
          },
          500:{
            'description':'Returns status and message about error'
          }
        }
      }
    }
  }
}
