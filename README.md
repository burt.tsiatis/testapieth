# For building and running local
```cli
npm run build
npm start
```

# For building and running via docker
```cli
docker-compose up -d --build
```

# Request to API on local
```cURL
curl --request GET --url http://localhost:9091/api/v1/getAddrFromBlock --header 'content-type: application/json'
```
# Swagger on uri
```
/api-docs
```